# mr_resturant

Welcome to my restaurants project built with Flutter! My app provides a seamless user experience for browsing our menu and resturants . I've implemented the Model-View-Controller (MVC) architecture pattern to ensure a clean separation of concerns and easy maintainability. My app uses the popular GetX package for state management and dependency injection, providing a fast and efficient user experience.

We've also integrated with Google Maps to provide accurate location information and directions to our restaurant. With the ability to view our restaurant location, hours of operation, and menu items with pictures and prices, customers can easily find what they're looking for quickly and easily.

Whether you're a foodie looking for your next culinary adventure or just looking for a quick and delicious meal, My restaurants app has got you covered. We're committed to delivering the best possible experience for our customers, and we hope you enjoy using our app as much as we enjoyed building it!

